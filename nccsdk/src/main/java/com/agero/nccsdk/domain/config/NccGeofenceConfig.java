package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/31/17.
 */

public class NccGeofenceConfig extends NccAbstractConfig {

    private int radius = 200; // Default 200 meters

    public NccGeofenceConfig() {
        super();
    }

    public NccGeofenceConfig(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
