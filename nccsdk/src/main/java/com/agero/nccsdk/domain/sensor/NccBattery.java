package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccBatteryData;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccBattery extends NccAbstractBroadcastReceiverSensor {

    private NccBatteryData lastBatteryReading;

    /**
     * Creates an instance of NccBattery
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     */
    public NccBattery(Context context, NccConfig config) {
        super(context, NccBattery.class.getSimpleName(), NccSensorType.BATTERY, config);

        // Initialize a last reading
        lastBatteryReading = new NccBatteryData(System.currentTimeMillis(), -1, false);
    }

    @Override
    String[] getActions() {
        return new String[]{
            Intent.ACTION_BATTERY_CHANGED
        };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        // Check if the device is currently charging
        // This check is specifically for when the phone is charging, and then the app is killed.
        // In this scenario, we don't know the current charging state.
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        NccBatteryData currentData = new NccBatteryData(System.currentTimeMillis(), level, isCharging);

        // Check last battery reading before posting
        if (lastBatteryReading.getLevel() != currentData.getLevel() ||
                lastBatteryReading.isCharging() != currentData.isCharging()) {
            postData(currentData);
        }
        lastBatteryReading = currentData;
    }
}
