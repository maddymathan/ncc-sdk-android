package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccAbstractNativeConfig;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.internal.log.Timber;

import java.util.concurrent.TimeUnit;

/**
 * Created by james hermida on 8/21/17.
 *
 * Abstract class to serve as a base class for all sensors provided by {@link SensorManager}
 */

abstract class NccAbstractNativeSensor extends NccAbstractSensor {

    private final String TAG;

    private final SensorManager mSensorManager;
    private final Sensor mSensor;
    private final SensorEventListener mSensorEventListener;
    private Handler mHandler;
    private HandlerThread mHandlerThread;

    private long firstSensorEventTimeMillis = 0, firstSensorEventTimeNanos = 0;

    /**
     * Creates an instance of NccAbstractNativeSensor. Invoked by a subclass's constructor.
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     * @param tag Used as a tag for subclasses only for debugging purposes
     * @param sensorType The type of sensor represented by {@link NccSensorType}
     * @throws NccException Exception if there is an error constructing the instance
     */
    NccAbstractNativeSensor(Context context, String tag, NccSensorType sensorType, NccConfig config) throws NccException {
        super(context, sensorType, config);
        TAG = tag;

        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(getNativeSensorType(sensorType));
        mSensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                setFirstSensorEventTimes(event);
                long calculatedEventTimeMillis = calculateSensorEventTimeMillis(event);

                NccAbstractSensorData data = buildData(event, calculatedEventTimeMillis);
                postData(data);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }

    /**
     * Abstract method implemented by a subclass to build the {@link NccAbstractSensorData} from Android's {@link SensorEvent}
     *
     * @param sensorEvent Android's SensorEvent representing the sensor data
     * @param sensorEventTime The calculated current time in milliseconds. See {@link #calculateSensorEventTimeMillis(SensorEvent)}.
     *                        {@link SensorEvent} timestamp is the elapsed time in nanoseconds since boot.
     * @return NccAbstractSensorData of the built data
     */
    protected abstract NccAbstractSensorData buildData(SensorEvent sensorEvent, long sensorEventTime);

    /**
     * See {@link NccSensor#startStreaming()}
     */
    @Override
    public void startStreaming() {
        this.isStreaming = true;

        startHandlers();

        NccAbstractNativeConfig config = (NccAbstractNativeConfig) this.config;
        if (!mSensorManager.registerListener(mSensorEventListener, mSensor, config.getSamplingRate(), mHandler)) {
            Timber.e("Sensor not registered: %s", sensorType.getName());
        }
    }

    /**
     * See {@link NccSensor#stopStreaming()}
     */
    @Override
    public void stopStreaming() {
        this.isStreaming = false;

        mSensorManager.unregisterListener(mSensorEventListener);

        stopHandlers();

        resetFirstEventTimes();
    }

    private void startHandlers() {
        mHandlerThread = new HandlerThread(TAG + HandlerThread.class.getSimpleName());
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    private void stopHandlers() {
        if (mHandlerThread != null && mHandlerThread.isAlive()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Timber.v("%s sensor event handler thread quit safely result: %s", sensorType.getName(), mHandlerThread.quitSafely());
            } else {
                Timber.v("%s sensor event handler thread quit result: %s", sensorType.getName(), mHandlerThread.quit());
            }
        }
    }

    /**
     * Gets the native sensor type defined by Android's {@link Sensor}
     *
     * @param sensorType The NccSensorType to get the native sensor type for
     * @return int of the native sensor type
     * @throws NccException Exception if the sensorType is not defined in NccSensorType
     */
    private int getNativeSensorType(NccSensorType sensorType) throws NccException {
        switch (sensorType) {
            case ACCELEROMETER:
                return Sensor.TYPE_ACCELEROMETER;
            case LINEAR_ACCELEROMETER:
                return Sensor.TYPE_LINEAR_ACCELERATION;
            case GYROSCOPE:
                return Sensor.TYPE_GYROSCOPE;
            case GYROSCOPE_UNCALIBRATED:
                return Sensor.TYPE_GYROSCOPE_UNCALIBRATED;
            case MAGNETIC_FIELD:
                return Sensor.TYPE_MAGNETIC_FIELD;
            case MAGNETIC_FIELD_UNCALIBRATED:
                return Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED;
            case ROTATION_VECTOR:
                return Sensor.TYPE_ROTATION_VECTOR;
            case BAROMETER:
                return Sensor.TYPE_PRESSURE;
            default:
                throw new NccException(TAG, "Unknown sensor type: " + sensorType.getName(), NccExceptionErrorCode.UNKNOWN_ERROR);
        }
    }

    private long calculateSensorEventTimeMillis(SensorEvent currentSensorEvent) {
        return TimeUnit.NANOSECONDS.toMillis((currentSensorEvent.timestamp - firstSensorEventTimeNanos)) + firstSensorEventTimeMillis;
    }

    private void setFirstSensorEventTimes(SensorEvent sensorEvent) {
        if (firstSensorEventTimeMillis == 0) {
            firstSensorEventTimeMillis = System.currentTimeMillis();
        }
        if (firstSensorEventTimeNanos == 0) {
            firstSensorEventTimeNanos = sensorEvent.timestamp;
        }
    }

    private void resetFirstEventTimes() {
        firstSensorEventTimeMillis = 0;
        firstSensorEventTimeNanos = 0;
    }
}
