package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/29/17.
 */

public class NccMagneticFieldData extends NccAbstractSensorData {

    private final float biasX;
    private final float biasY;
    private final float biasZ;
    private final int accuracy;

    /**
     * Creates an instance of NccMagneticFieldData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param biasX The earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     * @param biasY The earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     * @param biasZ The earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     * @param accuracy The accuracy of this data where -1 is uncalibrated, 0 is low accuracy, 1 is medium accuracy and 2 is high accuracy
     */
    public NccMagneticFieldData(long timestamp, float biasX, float biasY, float biasZ, int accuracy) {
        super(timestamp);
        this.biasX = biasX;
        this.biasY = biasY;
        this.biasZ = biasZ;
        this.accuracy = accuracy;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     */
    public float getBiasX() {
        return biasX;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     */
    public float getBiasY() {
        return biasY;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     */
    public float getBiasZ() {
        return biasZ;
    }

    /**
     * Gets the accuracy of this data where -1 is uncalibrated, 0 is low accuracy, 1 is medium accuracy and 2 is high accuracy
     *
     * @return int of the accuracy
     */
    public int getAccuracy() {
        return accuracy;
    }

    @Override
    public String toString() {
        return "NccMagneticFieldData{" +
                "biasX=" + biasX +
                ", biasY=" + biasY +
                ", biasZ=" + biasZ +
                ", accuracy=" + accuracy +
                ", timestamp=" + timestamp +
                '}';
    }
}
