package com.agero.nccsdk.domain.data;

import android.location.Location;

import com.google.android.gms.location.Geofence;

import java.util.List;

/**
 * Created by james hermida on 11/30/17.
 */

public class NccGeofenceEventData extends NccAbstractSensorData {

    private final int errorCode;
    private final int geofenceTransition;
    private final List<Geofence> triggeringGeofences;
    private final Location triggeringLocation;
    private final boolean hasError;

    public NccGeofenceEventData(long timestamp, List<Geofence> triggeringGeofences, Location triggeringLocation, int geofenceTransition, boolean hasError, int errorCode) {
        super(timestamp);
        this.triggeringGeofences = triggeringGeofences;
        this.triggeringLocation = triggeringLocation;
        this.geofenceTransition = geofenceTransition;
        this.hasError = hasError;
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public int getGeofenceTransition() {
        return geofenceTransition;
    }

    public List<Geofence> getTriggeringGeofences() {
        return triggeringGeofences;
    }

    public Location getTriggeringLocation() {
        return triggeringLocation;
    }

    public boolean isHasError() {
        return hasError;
    }
}
