package com.agero.nccsdk.adt;

import com.agero.nccsdk.internal.common.statemachine.State;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;

public interface AdtState extends State {

    void startMonitoring(StateMachine<AdtState> machine);
    void stopMonitoring(StateMachine<AdtState> machine);
    void startDriving(StateMachine<AdtState> machine);
    void stopDriving(StateMachine<AdtState> machine);
}
