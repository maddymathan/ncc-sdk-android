package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMagneticFieldUncalibratedData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccMagneticFieldUncalibratedMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccMagneticFieldUncalibratedData mfud = (NccMagneticFieldUncalibratedData) data;
        clearStringBuilder();
        sb.append("RM,");                               // Code
        sb.append(mfud.getBiasX()); sb.append(",");     // Bx
        sb.append(mfud.getBiasY()); sb.append(",");     // By
        sb.append(mfud.getBiasZ()); sb.append(",");     // Bz
        sb.append(mfud.getAccuracy()); sb.append(",");  // Baccuracy
        sb.append(mfud.getIronX()); sb.append(",");     // Ix
        sb.append(mfud.getIronY()); sb.append(",");     // Iy
        sb.append(mfud.getIronZ()); sb.append(",");     // Iz
        sb.append("NA,NA,");
        sb.append(mfud.getTimestamp()); sb.append(","); // UTCTime
        sb.append(getReadableTimestamp(mfud.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
