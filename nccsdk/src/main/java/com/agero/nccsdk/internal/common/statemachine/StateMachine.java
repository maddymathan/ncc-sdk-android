package com.agero.nccsdk.internal.common.statemachine;

/**
 * Created by james hermida on 11/6/17.
 */

public interface StateMachine<T extends State> {

    void setState(T newState);

}
