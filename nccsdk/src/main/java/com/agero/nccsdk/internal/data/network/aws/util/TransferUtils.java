package com.agero.nccsdk.internal.data.network.aws.util;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;

/**
 * Created by james hermida on 10/19/17.
 */

public class TransferUtils {

    public static String getTransferStateReadableString(TransferState transferState) {
        if (transferState == TransferState.WAITING) {
            return "WAITING";
        } else if (transferState == TransferState.IN_PROGRESS) {
            return "IN_PROGRESS";
        } else if (transferState == TransferState.PAUSED) {
            return "PAUSED";
        } else if (transferState == TransferState.RESUMED_WAITING) {
            return "RESUMED_WAITING";
        } else if (transferState == TransferState.COMPLETED) {
            return "COMPLETED";
        } else if (transferState == TransferState.CANCELED) {
            return "CANCELED";
        } else if (transferState == TransferState.FAILED) {
            return "FAILED";
        } else if (transferState == TransferState.WAITING_FOR_NETWORK) {
            return "WAITING_FOR_NETWORK";
        } else if (transferState == TransferState.PART_COMPLETED) {
            return "PART_COMPLETED";
        } else if (transferState == TransferState.PENDING_CANCEL) {
            return "PENDING_CANCEL";
        } else if (transferState == TransferState.PENDING_PAUSE) {
            return "PENDING_PAUSE";
        } else if (transferState == TransferState.PENDING_NETWORK_DISCONNECT) {
            return "PENDING_NETWORK_DISCONNECT";
        } else {
            return "UNKNOWN";
        }
    }

}
